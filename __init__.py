# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import category
from . import event


def register():
    Pool.register(
        category.ListDisease,
        category.EventCategory,
        event.Event,
        event.Employee,
        event.EventRegisterStart,
        module='staff_event', type_='model')
    Pool.register(
        event.EventRegister,
        module='staff_event', type_='wizard')
    Pool.register(
        event.EventReport,
        event.EventRegisterReport,
        module='staff_event', type_='report')
